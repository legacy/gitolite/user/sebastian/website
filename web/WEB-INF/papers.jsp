<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Tor Metrics Portal: Papers</title>
  <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
  <link href="/css/stylesheet-ltr.css" type="text/css" rel="stylesheet">
  <link href="/images/favicon.ico" type="image/x-icon" rel="shortcut icon">
</head>
<body>
  <div class="center">
    <%@ include file="banner.jsp"%>
    <div class="main-column">
        <h2>Tor Metrics Portal: Papers</h2>
        <br>
        <p>The <a href="#papers">papers</a>,
        <a href="#techreports">technical reports</a>, and
        <a href="#blogposts">blog posts</a> listed on this page originate
        from, are based on, or are related to work performed in the Tor
        Metrics Project.</p>
        <p><font color="red">This page will be removed soon.
        Its main purpose was to list metrics-related tech reports, but
        those have now moved
        <a href="https://research.torproject.org/techreports.html">here</a>.
        </font></p>
        <br>
        <a name="papers"></a>
        <h3><a href="#papers" class="anchor">Papers</a></h3>
        <br>
        These papers summarize some of the results of of the Tor Metrics
        Project and have been accepted for publication at academic
        conferences or workshops.
        <ul>
          <li>Karsten Loesing, Steven J. Murdoch, Roger Dingledine. A Case
          Study on Measuring Statistical Data in the Tor Anonymity
          Network. Workshop on Ethics in
          Computer Security Research (WECSR 2010), Tenerife, Spain,
          January 2010. (<a href="http://freehaven.net/anonbib/cache/wecsr10measuring-tor.pdf">PDF</a>)</li>
          <li>Karsten Loesing. Measuring the Tor Network from Public
          Directory Information. 2nd Hot Topics in Privacy Enhancing
          Technologies (HotPETs 2009), Seattle, WA, USA, August 2009.
          (<a href="https://research.torproject.org/techreports/metrics-2009-08-07.pdf">PDF</a>)</li>
        </ul>
        <br>
        <a name="techreports"></a>
        <h3><a href="#techreports" class="anchor">Technical
        reports</a></h3>
        <br>
        <p>
        Some of the
        <a href="https://research.torproject.org/techreports.html">Tor
        Technical Reports</a> have been the first place to
        publish novel kinds of statistics on the Tor network. Some, but
        not all, of the results contained in those technical reports have
        been included in the <a href="#papers">papers</a> above or in the
        daily updated <a href="graphs.html">graphs</a>.
        </p>
        <br>
        <a name="blogposts"></a>
        <h3><a href="#blogposts" class="anchor">Blog posts</a></h3>
        <br>
        The following blog posts are either the results of metrics
        research or describe new interesting research questions that can
        (partly) be answered with metrics data.
        <ul>
          <li>Research problems: Ten ways to discover Tor bridges
          (<a href="https://blog.torproject.org/blog/research-problems-ten-ways-discover-tor-bridges">link</a>,
          October 31, 2011).</li>
          <li>Research problem: better guard rotation parameters
          (<a href="https://blog.torproject.org/blog/research-problem-better-guard-rotation-parameters">link</a>,
          August 20, 2011).</li>
          <li>Research problem: measuring the safety of the Tor network
          (<a href="https://blog.torproject.org/blog/research-problem-measuring-safety-tor-network">link</a>,
          February 5, 2011).</li>
        </ul>
    </div>
  </div>
  <div class="bottom" id="bottom">
    <%@ include file="footer.jsp"%>
  </div>
</body>
</html>
